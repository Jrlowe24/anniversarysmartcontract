// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
// 0x6fe2eB2d047ae5341156220978F07AeE755Fb0d2
contract Anniversary {
  address public owner = msg.sender;
  uint256 id_start = 39485;
  uint256 curr_id = 39485;
  Entry[] entries;

  struct Entry {
    string person_1_first_name;
    string person_1_last_name;
    string person_2_first_name;
    string person_2_last_name;
    uint256 date;
    uint256 id;
  }

  function addEntry(string memory _p1fn, string memory _p1ln, string memory _p2fn, string memory _p2ln, uint256 date) public {
    Entry memory new_entry = Entry(_p1fn, _p1ln, _p2fn, _p2ln, date, curr_id);
    entries.push(new_entry);
    curr_id++;
  }

  function getEntries() public view onlyOwner() returns(Entry[] memory) {
    return entries;
  }

  modifier onlyOwner() {
    require(
      msg.sender == owner,
      "This function is restricted to the contract's owner"
    );
    _;
  }

}
